###############################################################################

def exercise_one():
    for i in range(1,101):
        if (i%5==0) and (i%3==0):
            print('ThreeFive')
        elif i%5 == 0:
            print('five')
        elif i%3==0 :
            print('three')
        else:
            print(i)

###############################################################################
def exercise_two(num):
    ##getting all substrings
    substrings_list=get_all_subtrings(num)
    result=[]

    ## loop for multiying the substrings
    for i in substrings_list:
        ## if one digit number
        if len(i)==1:
            result.append(i)
        else:
            mul=1
            ## loop for multiply
            for j in range(0,len(i)):
                mul=mul*int(i[j])
            result.append(mul)

    #if unique num == len of the list, true
    if len(set(result)) == len(substrings_list):
        return True
    # if contains doubles false
    else:
        return False


def get_all_subtrings(num):

    num_string=str(num)
    res = [num_string[i: j] for i in range(len(num_string))
          for j in range(i + 1, len(num_string) + 1)]
    return res

###########################################################################

def exercise_three(input):
#verify if the input is a list
  if not isinstance(input, list):
    return False

  result=0
  ## sum of the elements and verifiy if its a numbers
  for var in input :
    if isinstance(var,str) :
      if var.isnumeric() or (var[1:].isnumeric() and var.startswith('-')):
        result+= int(var)    
  return result

############################################################################

def contains(base,string):
  if base=='': 
    return True 
  if base[0] in string: 
    a=[c for c in string]
    a.remove(base[0])
    return contains(base[1:],''.join(a) )
  else :
    return False

def exercise_four(base,input):
  result=[]
  for var in input :    
    if len(base)==len(var):
        if contains(base,var):
          result.append(var)
  return result


#############################################################################