from django.http.response import  JsonResponse
from .models import Company, Tab, TabCompany, UserCom

#getting user infos
def get_user(request, id):

    #getting user with his id
    user=UserCom.objects.get(pk=id)

    #getting tabs related with the user and the company
    tabs=TabCompany.objects.filter(company=user.company)

    #getting the json of all infos
    result_list={'first_name':user.first_name,'last_name':user.last_name,'email':user.email,'company':user.company.name,'tabs':[t.tab.name for t in tabs]}

    

    return JsonResponse(result_list)


# getting all users with all infos
def get_users(request):

    users_list=UserCom.objects.all()
    result_list=[]

    for u in users_list:
        tabs=TabCompany.objects.filter(company=u.company)

        result_list.append({'first_name':u.first_name,'last_name':u.last_name,'email':u.email,'company':u.company.name,'tabs':[t.tab.name for t in tabs]})

    data={'users':result_list}

    return JsonResponse(data)