from django.contrib import admin
from .models import Company, Tab, TabCompany, UserCom


admin.site.register(Tab)
admin.site.register(TabCompany)
admin.site.register(UserCom)
admin.site.register(Company)