from django.db import models


class Company(models.Model):
    name = models.CharField(max_length=256)

    def __str__(self):
        return self.name

class Tab(models.Model):
    name = models.CharField(max_length=256)

    def __str__(self):
        return self.name

class TabCompany(models.Model):
    company =models.ForeignKey(Company, on_delete=models.CASCADE, blank=True, null=True)
    tab=models.ForeignKey(Tab, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.company.name + ' ' + self.tab.name


class UserCom(models.Model):
    first_name = models.CharField(max_length=256)
    last_name = models.CharField(max_length=256)
    email = models.EmailField()
    company = models.ForeignKey(Company, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.first_name + ' ' + self.last_name



