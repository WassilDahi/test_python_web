from django.http.response import  JsonResponse
from .models import Rank, Ship,Officer

def get_all_ships(request):
    ships=Ship.objects.all()
    temp_list=[]
    for s in ships:
        temp_list.append({'name':s.name,'captain':s.captain.name})
    data={'ships':temp_list}

    
    
    return JsonResponse(data)


def get_officers(request):
    ship_result=Ship.objects.filter(name='Ship 2')
    list_officers=Officer.objects.filter(ship_assignment__in=ship_result).order_by('order')
    
    temp_list=[]
    for o in list_officers:
        temp_list.append({'name':o.name,'order':o.order})

    data={'officers':temp_list}



    return JsonResponse(data)


def get_commanders(request):
    r=Rank.objects.filter(name='Commander')
    commanders_list=Officer.objects.filter(rank__in=r).order_by('order')

    temp_list=[]
    for c in commanders_list:
        s=Rank.objects.filter(officer=c)
        temp_list.append({'name':c.name,'order':c.order,'ranks':[a.name for a in s] })

    data={'officers':temp_list}


    return JsonResponse(data)