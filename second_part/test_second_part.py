import pytest

from src import div, raise_something, add, ForceToList, random_gen, get_info,CacheDecorator,checkProcess


def test_generator():
    g = random_gen()
    assert isinstance(g, type((x for x in [])))
    a = next(g)
    while a != 15:
        assert 10 <= a <= 20
        a = next(g)
    with pytest.raises(StopIteration):
        next(g)

#test_generator()

#####################################################

def test_to_str():
    print(add(5,30))
    assert add(5, 30) == '35'
    assert get_info({'info': [1, 2, 3]}) == '[1, 2, 3]'

#test_to_str()

#####################################################

def test_ignore_exception():
    assert div(10, 2) == 5
    assert div(10, 0) is None
    assert raise_something(TypeError) is None
    with pytest.raises(NotImplementedError):
        raise_something(NotImplementedError)

#test_ignore_exception()

#####################################################
def test_meta_list():
    test = ForceToList([1, 2])
    assert test[1] == 2
    assert test.x == 4

#test_meta_list()

########################################
@CacheDecorator()
def add(a,b):
    return a+b

@CacheDecorator()
def return_list(a,b):
    return[a,b]

@CacheDecorator()
def string_add(a,b):
    return str(a+b)

@CacheDecorator()
def return_multiple_output(a,b):
    return a+b,b


def test_cacheDecorator():
    assert add(1,2) == 3
    assert return_list(2,1) == [2,1]
    assert string_add(1,2) == '3'
    #The limite of decorator is that its couldnt return multiple outputs
    assert  return_multiple_output(3,2) == 5,2

#test_cacheDecorator()

###################################
class A(metaclass=checkProcess):
    x=1
    def process(a,b,c):
        return a+b+c
class B(metaclass=checkProcess):
    x=1
    def process(a,c):
        return a+c

class C(metaclass=checkProcess):
    x=1
    def processing(a,b,c):
        return a+b+c

def test_check_process():
    a=A()
    b=B()
    c=C()
    assert a.check == True
    assert b.check == False
    assert c.check == False

#test_cacheDecorator()