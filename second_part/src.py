from random import randrange

###################################################################################
#generating random numbers between 10 and 20 and stops just after giving 15
def random_gen():
    result=[]
    x=randrange(10,20)
    while (x!=15):
        result.append(x)
        x=randrange(10,20)
    
    return result

####################################################################################


#decorator that force to display string version of the output of function
def decorator_to_str(func):
    def inner(*args, **kwargs):
        ##converting output of function in string
       return str(func(*args, **kwargs))
    return inner


@decorator_to_str
def add(a, b):
    return a + b


@decorator_to_str
def get_info(d):
    return d['info']



#####################################################################################

def ignore_exception(func):

    def func_wrapper(*args, **kwargs):
        #ignoring the exception with try except
        try:
           return func(*args, **kwargs)

        except Exception as e:
            #veriying if type of the exception is 'NotImplementedError' to raise it especially
            if type(e).__name__ == 'NotImplementedError':
                
                raise NotImplementedError
            else:
                return None

    return func_wrapper


@ignore_exception
def div(a, b):
    return a / b


@ignore_exception
def raise_something(exception):
    raise exception

##########################################################################

# exercise 4
## i wrote the tests for this decorator, check the test file to see my comments
class CacheDecorator:
    """Saves the results of a function according to its parameters"""
    def __init__(self):
        self.cache = {}

    def __call__(self, func):
        
        def _wrap(*a, **kw):

            if a[0] not in self.cache:
                self.cache[a[0]] = func(*a, **kw)
                print(self.cache)
            return self.cache[a[0]]

        return _wrap


#######################################################################

class MetaInherList(type):
   def __new__(cls, name, bases, dct):
        ## added base+(list,) to force the class to be a list
         x = super().__new__(cls, name, bases+(list,), dct)
         return x    

class Ex:
    x = 4


class ForceToList(Ex, metaclass=MetaInherList):
    pass


########################################################################
#checks if classes have an attribute named 'process' which must be a method taking 3 arguments
class checkProcess(type):
   def __new__(cls, name, bases, dct):
        x = super().__new__(cls, name, bases, dct)
        ## check if attribute 'process' exists
        if 'process' in dct:
            ## count attributes and verify if its 3
            check_process=dct['process'].__code__.co_argcount
            if check_process==3:
                x.check=True
            else:
                x.check=False
        else:
            x.check=False
        return x

#########################################################################
